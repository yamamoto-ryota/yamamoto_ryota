package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;

@WebFilter(urlPatterns= {"/signup", "/management", "/setting"})
public class ManagementFillter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		List<String> errorMessages = new ArrayList<String>();
		int branchId = loginUser.getBranchId();
		int departmentId = loginUser.getDepartmentId();

		if(!(branchId == 1 && departmentId == 1)) {

			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);
		    ((HttpServletResponse) response).sendRedirect("./");

		}else {

			chain.doFilter(request, response);
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException {
			}

	@Override
	public void destroy() {
	}

}
