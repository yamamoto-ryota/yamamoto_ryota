package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.UserMessage;
import bbs.dao.MessageDao;
import bbs.dao.UserMessageDao;

public class MessageService {

    //新規投稿登録
    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
  //投稿表示
    public List<UserMessage> select(String start, String end, String word) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            Date current = new Date(Calendar.getInstance().getTime().getTime());
            String strCurrent = String.valueOf(current);

            String startTime = "";
            String endTime = "";
            String keyword = "";

            if(!StringUtils.isEmpty(start)) {
            	startTime = start + " 00:00:00";
            }else {
            	startTime = "2020-01-01 00:00:00";
            }

            if(!StringUtils.isEmpty(end)) {
            	endTime = end + " 23:59:59";
            }else {
            	endTime = strCurrent + " 23:59:59";
            }

            if(!StringUtils.isEmpty(word)) {
            	keyword = "%" + word + "%";
            }else {
            	keyword = "%%";
            }


            List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, startTime, endTime, keyword);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //投稿削除
    public void delete(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}