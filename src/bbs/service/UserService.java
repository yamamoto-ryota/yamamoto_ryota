package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.User;
import bbs.beans.UserBranchDepartment;
import bbs.dao.UserBranchDepartmentDao;
import bbs.dao.UserDao;
import bbs.utils.CipherUtil;

public class UserService {

    //新規登録
    public void insert(User user) {

        Connection connection = null;
        try {

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ログイン機能
    public User select(String account, String password) {

        Connection connection = null;
        try {

            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, account, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー重複チェック
    public User select(String account) {

        Connection connection = null;
        try {

            connection = getConnection();
            User user = new UserDao().select(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー情報
    public List<UserBranchDepartment> select() {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();
            List<UserBranchDepartment> users = new UserBranchDepartmentDao().select(connection, LIMIT_NUM);
            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //ユーザー編集
    //編集画面表示
    public User select(int selectedUserId) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, selectedUserId);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //編集内容投稿
    public void update(User user) {

        Connection connection = null;
        try {

        	if(!StringUtils.isEmpty(user.getPassword())) {

        		String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);

        	}

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //復活停止
    public void restartOrStop(User selectedUser) {

        Connection connection = null;
        try {
            connection = getConnection();
            new UserDao().restartOrStop(connection, selectedUser);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
