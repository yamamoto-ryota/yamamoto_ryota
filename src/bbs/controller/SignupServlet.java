package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	 HttpSession session = request.getSession();
    	 List<Branch> branches = new BranchService().select();
         session.setAttribute("branches", branches);

         List<Department> departments = new DepartmentService().select();
         session.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        List<String> errorMessages = new ArrayList<String>();

        String confirmation = request.getParameter("confirmation");
        User user = getUser(request);

        if (!isValid(user, confirmation, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }
        new UserService().insert(user);
        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
        return user;

    }

    private boolean isValid(User user, String confirmation, List<String> errorMessages) {

        String account = user.getAccount();
        String password = user.getPassword();
        String name = user.getName();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        User duplicateUser = new UserService().select(account);

        //アカウント
        if(StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if(20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if(6 > account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        }

        if(!StringUtils.isEmpty(account)) {

            if(!account.matches("^[A-Za-z0-9]+$")) {
        	    errorMessages.add("アカウント名は半角英数字で入力してください");
            }
        }

        if(duplicateUser != null) {
        	errorMessages.add("アカウントが重複しています");
        }

        //名前
        if(StringUtils.isBlank(name)) {
            errorMessages.add("名前を入力してください");
        }else if(10 < name.length()) {
        	errorMessages.add("名前は10文字以下で入力してください");
        }

        //パスワード
        if(StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }else if(20 < password.length()) {
            errorMessages.add("パスワードは20文字以下で入力してください");
        } else if(6 > password.length()) {
        	errorMessages.add("パスワードは6文字以上で入力してください");
        }

        //パスワードと確認用パスワードが一致ているかチェック
        if(!password.equals(confirmation)) {
        	errorMessages.add("パスワードと確認用パスワードが一致しません");
        }

        //支社と部署の対応
        if((branchId != 1) && (departmentId < 3)) {
        	errorMessages.add("指定した支社に存在しない部署です");
        }

        if((branchId == 1) && (departmentId > 2)) {
        	errorMessages.add("指定した支社に存在しない部署です");
        }

        if (errorMessages.size() != 0) {
            return false;
        }

        return true;
    }
}
