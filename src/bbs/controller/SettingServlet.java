package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	HttpSession session = request.getSession();

    	//アドレスバーへの入力フォーマットチェック
        List<String> errorMessages = new ArrayList<String>();
        String strId = request.getParameter("selected_user_id");

        if((StringUtils.isEmpty(strId)) || !(strId.matches("^[0-9]+$"))) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("management");
            return;
        }

    	//数字が入力されていたら
        int selectedUserId = Integer.parseInt(request.getParameter("selected_user_id"));
        User selectedUser = new UserService().select(selectedUserId);

        //ユーザーが存在するかチェック
        if(selectedUser == null) {
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("management");
            return;
        }

        List<Branch> branches = new BranchService().select();
        List<Department> departments = new DepartmentService().select();

        request.setAttribute("selectedUser", selectedUser);
        request.setAttribute("branches", branches);
        request.setAttribute("departments", departments);

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();
        List<Branch> branches = new BranchService().select();
        List<Department> departments = new DepartmentService().select();
        int selectedUserId = Integer.parseInt(request.getParameter("selected_user_id"));
        User selectedUser = new UserService().select(selectedUserId);

        String confirmation = request.getParameter("confirmation");
        User user = getUser(request);
        if (isValid(user, selectedUser, confirmation, errorMessages)) {
            try {
                new UserService().update(user);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.setAttribute("selectedUser", user);
            request.setAttribute("branches", branches);
            request.setAttribute("departments", departments);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

        session.setAttribute("selectedUser", user);
        response.sendRedirect("management");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("selected_user_id")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
        return user;
    }

    private boolean isValid(User user, User selectedUser, String confirmation, List<String> errorMessages) {

        String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        User duplicateUser = new UserService().select(account);

        //アカウント
        if(StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if(20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if(6 > account.length()) {
        	errorMessages.add("アカウント名は6文字以上で入力してください");
        }

        if(!StringUtils.isEmpty(account)) {

            if(!account.matches("^[A-Za-z0-9]+$")) {
        	    errorMessages.add("アカウント名は半角英数字で入力してください");
            }
        }

        if((duplicateUser != null) && (duplicateUser.getId() != selectedUser.getId())) {
        	errorMessages.add("アカウントが重複しています");
        }

        //名前
        if(StringUtils.isBlank(name)) {
            errorMessages.add("名前を入力してください");
        }else if(10 < name.length()) {
        	errorMessages.add("名前は10文字以下で入力してください");
        }

        //パスワード
        if(!StringUtils.isEmpty(password)) {

            if(20 < password.length()) {
               errorMessages.add("パスワードは20文字以下で入力してください");
            } else if(6 > password.length()) {
        	   errorMessages.add("パスワードは6文字以上で入力してください");
            }

        }
        //パスワードと確認用パスワードが一致ているかチェック
        if(!password.equals(confirmation)) {
        	errorMessages.add("パスワードと確認用パスワードが一致しません");
        }

        //支社と部署の対応
        if((branchId != 1) && (departmentId < 3)) {
        	errorMessages.add("指定した支社に存在しない部署です");
        }

        if((branchId == 1) && (departmentId > 2)) {
        	errorMessages.add("指定した支社に存在しない部署です");
        }

        if (errorMessages.size() != 0) {
            return false;
        }

        return true;

    }
}