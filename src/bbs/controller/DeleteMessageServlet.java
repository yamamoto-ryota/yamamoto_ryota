package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.Message;
import bbs.service.MessageService;

/**
 * Servlet implementation class DeleteMessage
 */
@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	        int id = Integer.parseInt(request.getParameter("id"));

	        Message message  = new Message();

	        message.setId(id);

	        new MessageService().delete(message);
	        response.sendRedirect("./");
	 }

}