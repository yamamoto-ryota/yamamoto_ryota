package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String start = request.getParameter("start");
        String end = request.getParameter("end");
        String word = request.getParameter("keyword");

        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.setAttribute("keyword", word);

        List<UserMessage> messages = new MessageService().select(start, end, word);
        request.setAttribute("messages", messages);

        List<UserComment> comments = new CommentService().select();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("home.jsp").forward(request, response);

    }
}
