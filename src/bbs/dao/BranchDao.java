package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Branch;
import bbs.exception.SQLRuntimeException;

public class BranchDao {

    public List<Branch> select(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("* ");
            sql.append("FROM branches ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<Branch> branches = toBranches(rs);
            return branches;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranches(ResultSet rs) throws SQLException {

        List<Branch> comments = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                Branch branch = new Branch();
                branch.setId(rs.getInt("id"));
                branch.setName(rs.getString("name"));
                branch.setCreatedDate(rs.getTimestamp("created_date"));
                branch.setUpdatedDate(rs.getTimestamp("updated_date"));

                comments.add(branch);
            }
            return comments;
        } finally {
            close(rs);
        }
    }
}
