package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserMessage;
import bbs.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, int num, String startTime, String endTime, String keyword) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" messages.id AS id,");
            sql.append(" messages.title AS title,");
            sql.append(" messages.category AS category,");
            sql.append(" messages.text AS text,");
            sql.append(" messages.user_id AS user_id,");
            sql.append(" users.account AS account,");
            sql.append(" users.name AS name,");
            sql.append(" messages.created_date AS created_date");
            sql.append(" FROM messages ");
            sql.append(" INNER JOIN users ");
            sql.append(" ON messages.user_id = users.id");
            sql.append(" WHERE messages.created_date");
            sql.append(" BETWEEN ? ");
            sql.append(" AND ? ");
            sql.append(" AND messages.category");
            sql.append(" LIKE ? ");
            sql.append(" ORDER BY created_date DESC limit " + num );

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startTime);
            ps.setString(2, endTime);
            ps.setString(3, keyword);

            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setTitle(rs.getString("title"));
                message.setCategory(rs.getString("category"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}
