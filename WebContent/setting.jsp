<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集画面</title>
        <link rel="stylesheet" href="general.css">
        <link rel="stylesheet" href="setting.css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <div class="back"><a href="management">ユーザー管理</a></div>

            <form action="setting" method="post"><br />
                <input name="selected_user_id" value="${selectedUser.id}" id="id" type="hidden"/>

                <label for="account">アカウント名</label><br />
                <input name="account" value="${selectedUser.account}" /><br />

                <label for="password">パスワード</label><br />
                <input name="password" type="password" id="password"/> <br />

                <label for="confirmation">確認用パスワード</label><br />
                <input name="confirmation" type="password" id="confirmation"/> <br />

                <label for="name">名前</label><br />
                <input name="name" value="${selectedUser.name}" id="name"><br />

                <c:if test="${loginUser.id != selectedUser.id}">
                  <label for="branch_id">支社</label><br />
                  <select name="branch_id">
                    <c:forEach items="${branches}" var="branch">
                       <c:if test="${selectedUser.branchId == branch.id}">
                          <option value="${branch.id}" selected>
                             <c:out value="${branch.name}" />
                          </option>
                       </c:if>
                       <c:if test="${selectedUser.branchId != branch.id}">
                          <option value="${branch.id}">
                             <c:out value="${branch.name}" />
                          </option>
                       </c:if>
                    </c:forEach>
                  </select><br />

                  <label for="department_id">部署</label><br />
                  <select name="department_id">
                    <c:forEach items="${departments}" var="department">
                      <c:if test="${selectedUser.departmentId == department.id}">
                         <option value="${department.id}" selected>
                            <c:out value="${department.name}" />
                         </option>
                      </c:if>
                      <c:if test="${selectedUser.departmentId != department.id}">
                         <option value="${department.id}">
                            <c:out value="${department.name}" />
                         </option>
                      </c:if>
                    </c:forEach>
                  </select><br />

                </c:if>

                <c:if test="${loginUser.id == selectedUser.id}">
                   <input type="hidden" name="branch_id" value="${selectedUser.branchId}" />
                   <input type="hidden" name="department_id" value="${selectedUser.departmentId}" />
                </c:if>
                <input type="submit" value="更新" /> <br />

            </form>

            <div class="copyright"> Copyright(c)Yamamoto Ryota</div>
        </div>
    </body>
</html>