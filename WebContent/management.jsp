<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー管理画面</title>
        <script type="text/javascript">

           function check(){

	         if(window.confirm('変更します。よろしいですか？')){
		     return true;

	         }else{

		     return false;

	         }
           }

        </script>

        <link rel="stylesheet" href="general.css">
        <link rel="stylesheet" href="management.css">

    </head>
    <body>
        <div class="main-contents">
            <div class="header">
                <a href="./">ホーム</a>
                <a href="signup">ユーザー新規登録</a>
            </div>
        </div>

        <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                   <c:forEach items="${errorMessages}" var="errorMessage">
                       <li><c:out value="${errorMessage}" />
                   </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
         </c:if>

        <div class="users">
           <c:forEach items="${users}" var="user">
               <div class="user">
                  <div class="account"><c:out value="${user.account}"/></div>
                  <div class="name"><c:out value="${user.name}" /></div>
                  <div class="branch_name"><c:out value="${user.branchName}"/></div>
                  <div class="department_name"><c:out value="${user.departmentName}" /></div>
                  <div class="is_stopped"><c:out value="${user.isStopped}" /></div><br />

                  <form action="setting" method="get">
					<input type="hidden" name="selected_user_id" value="${user.id}" />
                    <input type="submit" value="編集">
				  </form>

              <c:if test="${user.id != loginUser.id}">
                  <form action="stop" method="post" onSubmit="return check()">
                    <input type="hidden" name="selected_user_id" value="${user.id}" />
					<c:if test="${user.isStopped == 0 }" >
					   <input type="hidden" name="is_stopped" value="1" />
                       <input type="submit" value="停止">
					</c:if>

					<c:if test="${user.isStopped == 1 }" >
					   <input type="hidden" name="is_stopped" value="0" />
                       <input type="submit" value="復活">
					</c:if>

                  </form>
              </c:if>

                  <br />
               </div>


            </c:forEach>
        </div>
         <div class="copyright"> Copyright(c)Yamamoto Ryota</div>
    </body>
</html>