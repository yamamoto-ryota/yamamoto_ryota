<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
        <style type="text/css">

            .errorMessages{
	           color: #cc0000;
	           font-size: 16px;
            }

            body{
              padding-top: 2;
              background-color: #ccffff;
            }

            .main-contents{
	          margin: 0 auto;
              width: 800px;
              text-align: center;
              border: 3px double #999999;
            }

            .copyright{
	          text-align: left;
	          font-size: 12px;
            }

            input{
	          width: 150px;
	          padding: 5px 8px;
              border-radius: 6px;
              border-top: 1px solid #aaa;
              border-left: 1px solid #aaa;
              border-right: 2px solid #aaa;
              border-bottom: 2px solid #aaa;
              background-color: #ddd;
            }

        </style>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="login" method="post"><br />
                <label for="account">アカウント</label><br />
                <input name="account" id="account"/> <br />

                <label for="password">パスワード</label><br />
                <input name="password" type="password" id="password"/> <br />
                <br />

                <input type="submit" value="ログイン" /> <br />
            </form>

            <div class="copyright"> Copyright(c)Yamamoto Ryota</div>
        </div>
    </body>
</html>