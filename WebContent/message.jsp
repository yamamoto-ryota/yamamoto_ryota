<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規投稿画面</title>

    <link rel="stylesheet" href="general.css">
    <link rel="stylesheet" href="message.css">

    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <div class="back"><a href="./">ホーム</a></div>

            <form action="message" method="post"><br />

                <label for="title">件名</label><br />
                <input name="title" id="title" value="${message.title}" > <br />
                <br />

                <label for="category">カテゴリ</label><br />
                <input name="category" id="category" value="${message.category}" /> <br />
                <br />

                <label for="text">投稿内容</label><br />
                <textarea cols="50" rows="40" name="text" id="text" ></textarea><br />
                <br />

                <input type="submit" value="投稿" /> <br />
            </form>

            <div class="copyright">Copyright(c)Yamamoto Ryota</div>
        </div>
    </body>
</html>