<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ホーム画面</title>
        <script type="text/javascript">

           function check(){

	         if(window.confirm('削除してよろしいですか？')){

		       return true;

	         }else{

		       return false;

	         }
           }

        </script>

        <link rel="stylesheet" href="general.css">
        <link rel="stylesheet" href="home.css">

    </head>

    <body>
        <div class="main-contents">
            <div class="header">
                <a href="message">新規投稿</a>

                <c:if test="${loginUser.departmentId == 1}">
                  <a href="management">ユーザー管理</a>
                </c:if>

                <a href="logout">ログアウト</a>
            </div>
        </div>

           <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

        <div class="serch">
          <form action="./">
            <label for="date">日付</label>
            <input type="date"  name="start" value="${start}" />～<input type="date"  name="end" value="${end}" /><br />

            <label for="keyword">カテゴリ</label>
            <input name="keyword" value="${keyword}" />

            <input type="submit" value="絞込み" /><br />
          </form>
        </div>

        <div class="messages">
           <c:forEach items="${messages}" var="message">
               <div class="message">
                  <div class="name"><c:out value="${message.name}" /></div>
                  <div class="title"><c:out value="${message.title}"/></div>
                  <div class="category"><c:out value="${message.category}"/></div>
                   <c:forEach var="text" items="${fn:split(message.text, '
                       ')}">
                           <div class="text"><c:out value="${text}"/></div>
                       </c:forEach>
                  <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br />

                  <c:if test="${loginUser.id == message.userId}">
                      <form action="deleteMessage" method="post" onSubmit="return check()"> <br />
                         <input type="hidden" name="id" value="${message.id}" />

                         <input type="submit" value="削除" />
                      </form>
                  </c:if>

                  <br />

               </div>

                <c:forEach items="${comments}" var="comment">
                  <c:if test="${comment.messageId == message.id}">
                    <div class="comments">
                       <div class="user_name"><c:out value="${comment.name}"/></div>
                       <c:forEach var="text" items="${fn:split(comment.text, '
                       ')}">
                           <div class="text"><c:out value="${text}"/></div>
                       </c:forEach>
                       <div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br />
                       <c:if test="${loginUser.id == comment.userId}">
                           <form action="deleteComment" method="post" onSubmit="return check()"> <br />
                              <input type="hidden" name="id" value="${comment.id}" />

                              <input type="submit" value="削除" />
                           </form>
                       </c:if>
                    </div>
                  </c:if>
                </c:forEach>

                  <form action="comment" method="post"> <br />
                      <label for="comment">コメント入力欄</label> <br />
                      <pre><textarea cols="30" rows="20" name="comment" id="comment" ></textarea></pre>

                      <input type="hidden" name="message_id" value="${message.id}" />

                      <input type="submit" value="コメント投稿" /> <br />
                  </form>
                  <br />

            </c:forEach>
        </div>
         <div class="copyright"> Copyright(c)Yamamoto Ryota</div>
    </body>
</html>